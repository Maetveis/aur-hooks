"""Hook for checking that PKGBUILDs match .SRCINFOs"""

from typing import Annotated, ParamSpec, TypeVar, cast

import enum
import logging
import shutil
import subprocess
from collections.abc import Callable, Sequence
from functools import wraps
from pathlib import Path

import typer
from rich.logging import RichHandler

app = typer.Typer()

logger = logging.getLogger(__name__)


class _LogLevel(enum.StrEnum):
    DEBUG = "DEBUG"
    ERROR = "ERROR"
    INFO = "INFO"


class _ExitCode(enum.Enum):
    SUCCESS = 0
    CHECK_FAILED = 1
    ERROR = 2

    def merge(self, rhs: "_ExitCode") -> "_ExitCode":
        return self.__class__(max(self.value, rhs.value))


P = ParamSpec("P")
T_Co = TypeVar("T_Co", covariant=True)


def _log_cmd(f: Callable[P, T_Co]) -> Callable[P, T_Co]:
    @wraps(f)
    def inner(*args: P.args, **kwargs: P.kwargs) -> T_Co:
        cmd: Sequence[str] | None
        if "args" in kwargs:
            cmd = cast(Sequence[str], kwargs["args"])
        elif len(args) >= 1:
            cmd = cast(Sequence[str], args[0])
        else:
            raise TypeError("Expected command as first or keyword arguement")

        cwd: str | Path | None
        if "cwd" in kwargs:
            cwd = Path(cast(str | Path, kwargs["cwd"]))

        logger.debug(
            "Running command: '%s' with cwd='%s'",
            " ".join(cmd),
            cwd,
            stacklevel=2,
        )
        return f(*args, **kwargs)

    return inner


_run_cmd = _log_cmd(subprocess.run)


SRCINFO = ".SRCINFO"
PKGBUILD = "PKGBUILD"


@app.command()
def main(
    files: Annotated[
        list[Path],
        typer.Argument(
            help="Files to check (.SRCINFO or PKGBUILD)",
            exists=True,
            dir_okay=False,
            show_default=False,
        ),
    ],
    verify: Annotated[
        bool, typer.Option(help="Run makepkg --verifysource on the PKGBUILD")
    ] = True,
    makepkg_executable: Annotated[
        str, typer.Option(metavar="EXECUTABLE")
    ] = "makepkg",
    log_level: Annotated[_LogLevel, typer.Option()] = _LogLevel.INFO,
) -> None:
    """Check that PKGBUILDs match the correspoding .SRCINFOS"""
    logging.basicConfig(
        level=log_level.value,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler()],
    )

    makepkg_cmd = shutil.which(makepkg_executable)
    if makepkg_cmd is None:
        logger.error(
            "Command '%s' not found, make sure it is on the path, or set it via --makepkg-executable",
            makepkg_executable,
        )
        raise typer.Exit(_ExitCode.ERROR.value)

    package_folders = dict.fromkeys(file.parent.resolve() for file in files)
    logger.debug(
        "Will check package folders: %s",
        ", ".join(str(f) for f in package_folders),
    )

    exit_code = _ExitCode.SUCCESS
    for folder in package_folders:
        logger.info("Checking folder: %s", str(folder))
        if not folder.joinpath(PKGBUILD).is_file():
            logger.error("Directory %s does not contain PKGBUILD", str(folder))
            exit_code = exit_code.merge(_ExitCode.ERROR)
            continue
        if verify and (
            _run_cmd(
                (makepkg_cmd, "--verifysource", "--force"), cwd=folder
            ).returncode
            != 0
        ):
            exit_code = exit_code.merge(_ExitCode.CHECK_FAILED)
            continue

        srcinfo = folder / SRCINFO
        with srcinfo.open("w", encoding="utf-8") as srcinfo_f:
            if (
                _run_cmd(
                    (makepkg_cmd, "--printsrcinfo", "--force"),
                    cwd=folder,
                    stdout=srcinfo_f,
                ).returncode
                != 0
            ):
                exit_code = exit_code.merge(_ExitCode.CHECK_FAILED)
                srcinfo_f.close()
                srcinfo.unlink()

    raise typer.Exit(exit_code.value)


if __name__ == "__main__":
    app()
