python := "python"
pdm := "pdm"

verbose_errors := "false"

set windows-shell := ["powershell.exe", "-c"]

pdm-version := "2.10.3"
pdm-install-url    := "https://raw.githubusercontent.com/pdm-project/pdm/{{pdm-version}}/install-pdm.py"
pdm-install-sha256 := "10ec21aac4ad2fd38ffa506c16c9e1313a88b7f43f6b5ae11f75ab69dce965d7"

[unix]
install-pdm:
    curl --fail --location --silent --show-error --output install-pdm.py \
        https://raw.githubusercontent.com/pdm-project/pdm/cc3e908272568d0b8618f58c8df8ea30de6f73f9/install-pdm.py
    printf "%s %s\n" "{{pdm-install-sha256}}" "install-pdm.py" | sha256sum --check
    "{{python}}" install-pdm.py
    rm install-pdm.py

# Install development and runtime dependencies
install:
    "{{pdm}}" install

# (Re-)lock the dependencies with pip-compile
lock-deps +extra_args="":
	"{{pdm}}" lock --refresh {{extra_args}}

# Install git-hooks for development
install-git-hooks:
	"{{pdm}}" run pre-commit install --install-hooks

# Set up a development environment
devenv: install install-git-hooks

build:
	"{{pdm}}" build

_format extra_args +files:
	"{{pdm}}" run black --config pyproject.toml {{extra_args}} {{files}}

format +files="src": (_format "" files)
check-format +files="src": (_format "--check" files)

_isort extra_args +files:
	"{{pdm}}" run isort --settings-path pyproject.toml {{extra_args}} {{files}}

# Sort imports
isort +files="src": (_isort "" files)
# Check if imports are correctly sorted
check-isort +files="src": (_isort "--check" files)

_ruff extra_args +files:
	"{{pdm}}" run ruff --config pyproject.toml {{extra_args}} {{files}}

# Run ruff to lint files
ruff +files="src": (_ruff "" files)
# Run ruff and autolint
fix-ruff +files="src": (_ruff "--fix --exit-non-zero-on-fix" files)

_run-hook hook:
	@"{{pdm}}" run pre-commit run --all-files {{ if verbose_errors == "true" {"--show-diff-on-failure"} else {""} }} {{hook}}

# Run basic pre-commit hooks
hooks: (_run-hook "check-yaml") (_run-hook "check-toml") (_run-hook "end-of-file-fixer") (_run-hook "trailing-whitespace")

# Run linters, no files are modified
lint +files="src": (ruff "" files) hooks
# Run linters, trying to fix errors automatically
fix-lint +files="src": (fix-ruff files) hooks

# Run type-checking
mypy +files="src":
	"{{pdm}}" run mypy --config-file pyproject.toml {{files}}

# Run formatting, linters, imports sorting, fixing errors if possible
fix-codestyle +files="src": (fix-lint files) (isort files) (format files)
alias codestyle := fix-codestyle

# Check formatting, linters, imports sorting
check-codestyle +files="src": (lint files) (check-isort files) (check-format files) (mypy files)
alias check := check-codestyle

# test +files="src":
# 	{{python}} -m pytest {{files}}

_check-commit-mesg file:
	"{{pdm}}" run cz check --allow-abort --commit-msg-file {{file}}

_check-commit-range range:
	"{{pdm}}" run cz check --rev-range {{range}}
